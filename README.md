# grunt-direct-ngc
Async Grunt task to call ngc from the command line, but without forking a new node

You can call ngc from Grunt as you learned it from the command line. However, it is called
as an ordinary Grunt task, without forking a new node instance. Despite that, you can
safely give any command line options you used.

An example:

    module.exports = function(grunt) {
      'use strict';
      
      grunt.initConfig({
      
        ngc : {
          options : {
            args : ["-p", "tsconfig-aot.json"]
          }
        }
      
      }
      
      grunt.loadNpmTasks("grunt-direct-ngc");
    };

It will be the equivalent of a `./node_modules/.bin/ngc -p tsconfig-aot.json`. However,
it will be run from Grunt directly, without any forking.

Watchmodue (`-w`) is not supported yet, by if you open a github issue, I will implement it
in days.
