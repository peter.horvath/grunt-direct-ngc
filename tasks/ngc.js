"use strict";

//require('reflect-metadata');
var ngc = require('@angular/compiler-cli/src/main');

function invokeNgc(grunt, self) {
	self.done = self.async();

	var options = self.options({
		args: ["-p", "tsconfig.json"],
    debugArgs: false
	});

  if (options.debugArgs) {
  	grunt.log.debug("ngcmain start, args: " + JSON.stringify(options.args));
  }

	var exitCode = ngc.main(options.args);

  if (options.debugArgs) {
	  grunt.log.debug("ngcmain done, args were: " + JSON.stringify(options.args));
  }

	self.done(exitCode === 0);
}

function registerNgcTask(grunt) {
	grunt.registerTask("ngc", "invokes angular compiler (ngc) from node without forking, but as from command line", function() {
		invokeNgc(grunt, this);
	});
}

module.exports = registerNgcTask;
